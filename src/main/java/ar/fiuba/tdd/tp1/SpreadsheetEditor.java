package ar.fiuba.tdd.tp1;

public class SpreadsheetEditor implements Creator {
    private CreatedList<Document> documents;

    SpreadsheetEditor() {
        documents = new CreatedList<>();
    }

    @Override
    public CreatedList getCreatedsList() {
        return documents;
    }

    @Override
    public void create(String name) {
        Document document = new Document(name);
        this.documents.addAtEnd(document);
    }

    public Document getDocument(String docName) {
        return (Document)documents.get(docName);
    }

    public void setDocument(Document doc) {
        this.documents.addAtEnd(doc);
    }

    public void deleteDocument(Document doc) {
        Document docAux = getDocument(doc.getName());
        if (docAux != null) {
            documents.getList().remove(docAux);
        }
    }
}
