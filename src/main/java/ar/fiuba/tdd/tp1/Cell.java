package ar.fiuba.tdd.tp1;

public class Cell {
    private String name;
    private String strValue;
    private Expression value;
    private boolean evaluating;
    private Format format;

    Cell() {
        defaultValues();
    }

    public String getValue() {
        return value.toString();
    }

    public Cell(Cell other) {
        this.setValues(other);
    }

    public void setName(String someName) {
        name = someName;
    }

    public String getName() {
        return name;
    }

    public void setValue(Expression value, String src) {
        this.strValue = src;
        checkType(format.getType());
        if (value == null) {
            this.value = new StringType(src);
        } else {
            this.value = value;
        }
    }

    public double eval() {
        if (evaluating) {
            throw new BadReferenceException();
        }
        evaluating = true;
        if (value != null) {
            Double result = value.eval();
            evaluating = false;
            return result;
        }
        return 0;
    }

    public String getSrc() {
        return strValue;
    }

    public void setSrc(String string) {
        strValue = string;
    }

    public String getStringValue() {
        if (this.evaluating == true) {
            throw new BadReferenceException();
        }
        evaluating = true;
        if (this.value == null) {
            return "{0}";
        }
        String result = value.toString();
        evaluating = false;
        return result;
    }

    public void setValues(Cell other) {
        if (other == null) {
            defaultValues();
            return;
        }
        this.name = other.name;
        this.strValue = other.strValue;
        this.value = other.value;
        this.evaluating = other.evaluating;
        this.format = other.format;
    }

    private void defaultValues() {
        name = "cell";
        strValue = "";
        value = new StringType("");
        evaluating = false;
        format = new Format("StringType");
        format.setFormat(null);
        format.setFormater(null);
    }

    public void setType(String type, String formatter ,String format) {
        if ((type.compareTo(this.format.getType()) == 0) && (type.compareTo("currency") == 0)) {
            this.format.setChild(new Format(type, format, formatter));
        } else {
            this.format.setType(type);
            this.format.setFormater(formatter);
            this.format.setFormat(format);
        }
        this.setExpression();
    }

    private void setMoneyType() {
        Format child = this.format;
        do {
            if (child.getFormater() == "decimal") {
                ((Money) this.value).setQuantityOfDecimals(Integer.parseInt(child.getFormat()));
            } else {
                ((Money) this.value).setSymbol(child.getFormat());
            }
            child = child.getChild();
        } while ( child != null );
    }

    private void createType(Type foundType) {
        switch (foundType) {
            case currency: 
                this.value = new Money(value.toString());
                setMoneyType();
                break;
            case number:
                if (this.format.getFormat() != null) {
                    this.value = new Number(value.toString(), Integer.parseInt(this.format.getFormat()));
                } else {
                    this.value = new Number(value.toString());
                }
                break;
            case date: this.value = new Date(getStringValue(), this.format.getFormat());
                break;
            default: this.value = new StringType(value.toString());
                break;
        }
    }

    public void checkType(String type) {
        Type foundType = Type.string;
        for (Type typeAux : Type.values()) {
            if (typeAux.toString().compareTo(type.toString()) == 0) {
                foundType = typeAux;
                break;
            }
        }
        createType(foundType);
    }

    private void setExpression() {
        checkType(this.format.getType());
    }

    private void updateStringValue() {
        strValue = value.toString();
    }

    public void setFormatter(String formatter, String format) {
        ((Formatter) this.value).setFormatter(formatter, format);
        updateStringValue();
    }

    public Expression getExpression() {
        return value;
    }

    public Format getFormat() {
        return format;
    }
}