package ar.fiuba.tdd.tp1;

public interface BinaryOperator {

    double operate(double left, double right);

}
