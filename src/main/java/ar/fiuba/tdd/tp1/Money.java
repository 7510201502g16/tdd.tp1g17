package ar.fiuba.tdd.tp1;

public class Money implements Expression, Formatter {
    private Expression value;
    private String symbol;

    public Money(String value) {
        int count = 0;
        try {
            while (!Character.isDigit(value.charAt(count))) {
                count++;
            }
            symbol = value.substring(0, count);
            this.value = new Number(value.substring(count), 0);
        } catch (IndexOutOfBoundsException e) {
            this.value = new StringType("Error:BAD_CURRENCY");
        }
    }

    public Money(String value, int decimals) {
        int count = 0;
        while (!Character.isDigit(value.charAt(count))) {
            count++;
        }
        symbol = value.substring(0, count);
        if ((decimals == 0) || (decimals == 2)) {
            this.value = new Number(value.substring(count), decimals);
        } else {
            this.value = new Number(value.substring(count), 0);
        }
    }

    public String toString() {
        return (symbol + " " + value.toString());
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public void setValue(Number value) {
        int decimals = ((Number) this.value).getQuantityOfDecimals();
        this.value = value;
        setQuantityOfDecimals(decimals);
    }

    public void setQuantityOfDecimals(int quantity) {
        if ((quantity == 0) || (quantity == 2)) {
            ((Number) this.value).setQuantityOfDecimals(quantity);
        }
    }

    @Override
    public double eval() {
        return value.eval();
    }

    @Override
    public void setFormatter(String formatter, String format) {
        if (formatter.compareTo("symbol") == 0) {
            this.symbol = format;
        }
        if (formatter.compareTo("decimal") == 0) {
            this.setQuantityOfDecimals(Integer.parseInt(format));
        }
    }
}