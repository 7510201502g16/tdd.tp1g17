package ar.fiuba.tdd.tp1;

import java.util.Stack;

public class CommandHistory {
    private Stack<Command> redoStack;
    private Stack<Command> undoStack;

    CommandHistory() {
        redoStack = new Stack();
        undoStack = new Stack();
    }

    public Stack getRedoStack() {
        return redoStack;
    }

    public Stack getUndoStack() {
        return undoStack;
    }

    public void addCommandToRedoStack(Command command) {
        redoStack.push(command);
    }

    public void addCommandToUndoStack(Command command) {
        undoStack.push(command);
    }

    public void deleteCommandFromRedoStack() {
        if (redoStack.size() > 0) {
            redoStack.pop();
        }
    }

    public void deleteCommandFromUndoStack() {
        if (undoStack.size() > 0) {
            undoStack.pop();
        }
    }

    public void clearRedoStack() {
        redoStack.clear();
    }
}