package ar.fiuba.tdd.tp1;

import java.util.LinkedList;

/**
 * Created by matias on 11/5/15.
 */
public class LeftFunction extends SubstringFunction {

    public LeftFunction() {
        super();
    }

    public LeftFunction(LinkedList<Expression> values) {
        super(values);
    }


    public String toString() {
        Integer amount = this.castSecondArgument(this.secondArgument.toString());
        return this.firstArgument.toString().substring(0,amount);
    }

    @Override
    public Function getCopy() {
        return new LeftFunction(this.params);
    }
}
