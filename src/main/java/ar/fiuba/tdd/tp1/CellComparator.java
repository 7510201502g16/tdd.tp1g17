package ar.fiuba.tdd.tp1;

public class CellComparator implements Comparable<Object> {
    Position position;
    private Cell cell;

    public CellComparator(Position position, Cell cell) {
        this.position = position;
        this.cell = cell;
    }

    public Cell getCell() {
        return cell;
    }

    public Position getPosition() {
        return position;
    }

    @Override
    public boolean equals(Object otherCell) {
        if (this == otherCell) {
            return true;
        }
        return false;
    }

    @Override
    public int compareTo(Object otherCell) {
        int posRow = (int) position.getSecond();
        int posCol = (int) position.getFirst();
        int otherPosRow = (int) ((CellComparator) otherCell).position.getSecond();
        int otherPosCol = (int) ((CellComparator) otherCell).position.getFirst();
        if (posRow < otherPosRow) {
            return -1;
        } else {
            if ((posRow == otherPosRow) && (posCol < otherPosCol)) {
                return -1;
            }
        }
        if ((posRow == otherPosRow) && (posCol == otherPosCol)) {
            return 0;
        }
        return 1;
    }


        /*int posRow = (int) position.getSecond();
        int posCol = (int) position.getFirst();
        int otherPosRow = (int) otherCell.position.getSecond();
        int otherPosCol = (int) otherCell.position.getFirst();
        if ((int) position.getSecond() < (int) otherCell.position.getSecond()) {
            return -1;
        } else {
            if (((int) position.getSecond() == (int) otherCell.position.getSecond()) &&
                    ((int) position.getFirst() < (int) otherCell.position.getFirst())) {
                return -1;
            }
        }
        return 1;
    }*/
}
