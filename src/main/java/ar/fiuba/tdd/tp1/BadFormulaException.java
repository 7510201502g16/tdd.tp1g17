package ar.fiuba.tdd.tp1;

public class BadFormulaException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    String src;

    public BadFormulaException(String message, String src) {
        super(message);
        this.src = src;
    }

    public BadFormulaException() {
        super();
    }

    public String getMessage() {
        String msg = super.getMessage();
        return msg + ":" + src;
    }

}