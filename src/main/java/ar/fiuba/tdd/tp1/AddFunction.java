package ar.fiuba.tdd.tp1;

import java.util.LinkedList;

public class AddFunction extends GroupFunction {

    public AddFunction() {
        this.values = null;
    }
    
    public AddFunction(LinkedList<Expression> values) {
        saveParams(values);
    }

    @Override
    public Function getCopy() {
        return new AddFunction(this.values);
    }

    @Override
    protected void operate(double parRes) {
        result += parRes;
    }

    @Override
    protected void init() {
        this.result = 0;
    }

    @Override
    protected void end() {
    }

}
