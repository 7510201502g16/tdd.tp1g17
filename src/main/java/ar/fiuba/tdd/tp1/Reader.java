package ar.fiuba.tdd.tp1;

public interface Reader {

    String read();

}
