package ar.fiuba.tdd.tp1;

import java.util.LinkedList;

public class ConcatFunction implements Function, Expression {

    private LinkedList<Expression> params;

    public ConcatFunction(LinkedList<Expression> params) {
        this.params = params;
    }

    public ConcatFunction() {
        this.params = null;
    }

    @Override
    public Function getCopy() {
        return new ConcatFunction(this.params);
    }

    @Override
    public void saveParams(LinkedList<Expression> params) {
        this.params = params;
    }

    @Override
    public double eval() {
        throw new BadFormulaException();
    }

    public String toString() {
        String result = "";
        for (Expression arg : params) {
            result = result.concat(arg.toString());
        }
        return result;
    }

}
