package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.Tokenizer.Symbol;

public class SpreadSheetParser extends CellParser {

    public SpreadSheetParser() {
        super();
    }

    public Expression parse() throws BadFormulaException, UndeclaredWorkSheetException {
        // cellContent = <string> | <number> | <formula>
        Expression result = formulaOrDate();
        if (result != null) {
            return result;
        }
        NumberParser numParser = new NumberParser(this);
        result = numParser.parse();
        if (result == null) {
            result = new StringType(tokenizer.getSrc());
        }
        return result;
    }

    public Expression formulaOrDate() throws UndeclaredWorkSheetException {
        Expression result = formula();
        if (result != null) {
            return result;
        }
        return date();
    }
    
    private Expression date() {
        String result = tokenizer.getMatch(Symbol.DATE);
        if (result != null) {
            return new StringType(result);
        }
        return null;
    }

    Expression formula() throws BadFormulaException, UndeclaredWorkSheetException {
        // formula = "=" <expression>
        String result = tokenizer.getMatch(Symbol.EQUAL);
        if (result != null) {
            ExpressionParser parser = new ExpressionParser(this);
            return parser.parse();
        }
        return null;
    }
}