package ar.fiuba.tdd.tp1;

/**
 * Created by maxi on 07/11/15.
 */
public class StringParser extends CellParser {

    public StringParser(Parser parser) {
        super(parser);
    }

    @Override
    public Expression parse() {
        String result = tokenizer.getMatch(Tokenizer.Symbol.STRING);
        if ((result != null) && (result.length() > 1)) {
            return new StringType(result);
        }
        return null;
    }
}
