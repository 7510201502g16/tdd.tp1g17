package ar.fiuba.tdd.tp1;

public class Date implements Expression, Formatter {
    private int day;
    private int month;
    private int year;
    private DateFormat dateFormat;
    private String date;

    public Date(int day, int month, int year, String date) {
        this.day = day;
        this.month = month;
        this.year = year;
        validateDate();
        this.date = date;
        dateFormat = new DateFormatYearMonthDay();
    }

    public Date(String date) {
        createDate(date);
        this.date = date;
        this.dateFormat = null;
    }

    private void createDate(String date) {
        try {
            this.year = Integer.parseInt(date.substring(0, 4));
            this.month = Integer.parseInt(date.substring(5, 7));
            this.day = Integer.parseInt(date.substring(8, 10));
            validateDate();
        } catch (Exception e) {
            this.date = "Error:BAD_DATE";
            this.year = -1;
        }
    }

    public Date(String date, String format) {
        this.date = date;
        this.dateFormat = checkDateFormat(format, date);
    }

    public String getDayAsString() {
        return String.valueOf(day);
    }

    public String getMonthAsString() {
        return String.valueOf(month);
    }

    public String getYearAsString() {
        return String.valueOf(year);
    }

    private void validateFebruaryMaximumDay() {
        if (year % 4 == 0) {
            if (day > 29) {
                throw new DateException(DateException.messageDayOutOfRange);
            }
        } else {
            if (day > 28) {
                throw new DateException(DateException.messageDayOutOfRange);
            }
        }
    }

    private void validateNotFebruaryMaximumDay() {
        if ((month == 4) || (month == 6) || (month == 9) || (month == 11)) {
            if (day > 30) {
                throw new DateException(DateException.messageDayOutOfRange);
            }
        } else {
            if (day > 31) {
                throw new DateException(DateException.messageDayOutOfRange);
            }
        }
    }

    private void validateDayRange() {
        if (day < 1) {
            throw new DateException(DateException.messageDayOutOfRange);
        }
        if (month == 2) {
            validateFebruaryMaximumDay();
        } else {
            validateNotFebruaryMaximumDay();
        }
    }

    private void validateMonthRange() {
        if ((month < 1) || (month > 12)) {
            throw new DateException(DateException.messageMonthOutOfRange);
        }
    }

    private void validateYear() {
        if ((year < 0) || (year > 9999)) {
            throw new DateException(DateException.messageYearOutOfRange);
        }
    }

    private void validateDate() {
        validateDayRange();
        validateMonthRange();
        validateYear();
    }

    @Override
    public double eval() {
        return 0;
    }

    private DateFormat checkDateFormat(String format, String date) {
        createDate(date);
        if (this.year == -1) {
            return null;
        }
        loadFormatter(format);
        this.date = dateFormat.showDate(this, "-");
        return dateFormat;
    }

    public void loadFormatter(String format) {
        if (format != null) {
            format = format.toLowerCase();
            if (format.compareTo("dd-mm-yyyy") == 0) {
                dateFormat = new DateFormatDayMonthYear();
            } else {
                if (format.compareTo("mm-dd-yyyy") == 0) {
                    dateFormat = new DateFormatMonthDayYear();
                } else {
                    if (format.compareTo("yyyy-mm-dd") == 0) {
                        dateFormat = new DateFormatYearMonthDay();
                    }
                }
            }
        } else {
            dateFormat = new DateFormatYearMonthDay();
        }
    }

    @Override
    public void setFormatter(String formatter, String format) {
        if (this.year == -1) {
            return;
        }
        checkDateFormat(format, date);
    }

    public String toString() {
        if (year == -1) {
            return this.date;
        }
        if (dateFormat == null) {
            dateFormat = new DateFormatYearMonthDay();
        }

        return dateFormat.showDate(this, "-");

    }
}