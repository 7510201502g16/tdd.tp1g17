package ar.fiuba.tdd.tp1;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by maxi on 06/11/15.
 */
public class TodayFunction extends SingleFunction {

    public TodayFunction() {
        super();
    }

    @Override
    public Function getCopy() {
        return new TodayFunction();
    }

    public String toString() {
        java.util.Date date = new Date();
        java.text.DateFormat dateFormat = new SimpleDateFormat("YYYY-MM-dd");
        return dateFormat.format(date);
    }
}
