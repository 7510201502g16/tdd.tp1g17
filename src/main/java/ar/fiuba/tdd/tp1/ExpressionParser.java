package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.Tokenizer.Symbol;

public class ExpressionParser extends CellParser {

    public ExpressionParser(Parser parser) {
        super(parser);
    }

    @Override
    public Expression parse() throws BadFormulaException, UndeclaredWorkSheetException {
        // expression = term { ( "+" | "-" ) term }
        Expression left = term();
        while (!tokenizer.isEmpty()) {
            String result = tokenizer.getMatch(Symbol.ADD);
            if (result != null) {
                left = BinaryOperatorExpression.add(left, term());
                continue;
            }
            result = tokenizer.getMatch(Symbol.SUB);
            if (result != null) {
                left = BinaryOperatorExpression.sub(left, term());
                continue;
            }
            break;
        }
        return left;
    }
    
    private Expression term() throws BadFormulaException, UndeclaredWorkSheetException {
     // term = factor { ( "*" | "/" ) factor }
        Expression left = factor();
        while (!tokenizer.isEmpty()) {
            String result = tokenizer.getMatch(Symbol.STAR);
            if (result != null) {
                left = BinaryOperatorExpression.star(left, factor());
                continue;
            }
            //result = tokenizer.getMatch(Symbol.DIV);
            //if (result != null) {
            //    left = BinaryOperatorExpression.div(left, factor());
            //    continue;
            //}
            result = tokenizer.getMatch(Symbol.POW);
            if (result != null) {
                left = BinaryOperatorExpression.pow(left, factor());
                continue;
            }
            break;
        }
        return left;
    }
    
    private Expression factor() throws BadFormulaException, UndeclaredWorkSheetException {
        // factor = funcCall | "(" expression ")" | variable

        Expression result = funcCall();
        if (result != null) {
            return result;
        }
        result = enclosedByparensExp();
        if (result != null) {
            return result;
        }
        return variable();
    }
    
    private Expression funcCall() throws UndeclaredWorkSheetException {
        FunctionParser parser = new FunctionParser(this);
        return parser.parse();
    }

    public Expression enclosedByparensExp() throws BadFormulaException, UndeclaredWorkSheetException {
        String result = tokenizer.getMatch(Symbol.OPEN_PARENS);
        if (result != null) {
            Expression value = parse();
            String rparen = tokenizer.getMatch(Symbol.CLOSE_PARENS);
            if (value != null && rparen != null) {
                return new EnclosedByParensExp(value);
            }
        }
        return null;
    }
    
    public Expression variable() throws BadFormulaException, UndeclaredWorkSheetException {
        // variable = number | cellReference
        Expression result = cellReference();
        if (result == null) {
            NumberParser numParser = new NumberParser(this);
            result = numParser.parse() ;
            if (result == null) {
                StringParser stringParser = new StringParser(this);
                result = stringParser.parse() ;
                if (result == null) {
                    throw new BadFormulaException("Error parsing source", tokenizer.getSrc());
                }
            }
        }
        return result;
    }
    

    public CellReference cellReference() throws UndeclaredWorkSheetException {
        String result = tokenizer.getMatch(Symbol.CELLREF);
        if (result == null) {
            return null;
        }
        String[] names = result.split("\\.");
        Spreadsheet sheet = null;
        if (names.length == 3) {
            sheet = fss.getSheet(names[0], names[1]);
        } else if (names.length == 2) {
            Document doc = fss.getActualDoc();
            sheet = doc.getSpreadsheet(names[0]);
        } else {
            sheet = fss.getCurrentSheet();
        }
        String pos = names[names.length - 1];
        int[] xy = parseCellPos(pos);
        CellReference cellRef = new CellReference(sheet, xy[0], xy[1]);
        return cellRef;
    }

}
