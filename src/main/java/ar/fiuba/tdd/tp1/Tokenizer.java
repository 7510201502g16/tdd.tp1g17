package ar.fiuba.tdd.tp1;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Tokenizer {

    protected String src;
    private String parsed;
    
    public enum Symbol {

        OPEN_PARENS("\\("), 
        CLOSE_PARENS("\\)"), 
        ADD("[+]"), 
        SUB("[-]"), 
        STAR("[*]"), 
        DIV("[/]"),
        POW("[\\^]"),
        NUMBER("[-]?[0-9]+[.0-9]*"), // float number
        EQUAL("[=]"),
        CELLREF("([a-zA-Z][a-zA-Z0-9_]*[.]){0,2}([a-zA-Z]+[0-9]+)"), 
        CHARSEQ("[a-zA-Z]+"), 
        FUNC_NAME("[A-Z_]+\\s*\\("), 
        ARG_CONCAT("[,]"),
        RANGE_SEP("[:]"), 
        RANGE("([a-zA-Z]+[0-9]+)\\s*[:]\\s*([a-zA-Z]+[0-9]+)"),
        NOT_SPACE("[^\\s]*[ ]*"),
        STRING("\"([^\"]*)\""),
        DATE("[0-9]+[-][0-9]+[-][0-9]+.*"),
        COMM_NAME("[A-Z_]+\\s*");
        

        public String regex;

        Symbol(String regex) {
            this.regex = regex;
        }
        

    }
    
    public Tokenizer() {
        this.src = "";
        this.parsed = "";
    }

    public void setSrc(String src) {
        this.src = src;
        this.parsed = "";
    }

    public String getMatch(Symbol symbol) {
        Pattern pattern = getPattern(symbol);
        if (!isEmpty()) {
            Matcher matcher = pattern.matcher(src);
            if (matcher.find()) {

                String tok = matcher.group().trim();
                src = matcher.replaceFirst("");
                parsed += tok;
                return tok;
            }
        }
        return null;
    }

    public Pattern getPattern(Symbol symbol) {
        return Pattern.compile("^\\s*(" + symbol.regex + ")");
    }

    public boolean isEmpty() {
        Pattern pattern = Pattern.compile("^\\s*$");
        Matcher matcher = pattern.matcher(src);
        return matcher.find();
    }

    public String getSrc() {
        return parsed + src;
    }

    public String getRemainingSrc() {
        return src;
    }

    public boolean contains(Symbol seq) {
        Pattern pattern = getPattern(seq);
        Matcher matcher = pattern.matcher(src);
        return matcher.find();
    }

}
