package ar.fiuba.tdd.tp1;

import java.util.LinkedList;

/**
 * Created by maxi on 06/11/15.
 */
public abstract class SingleFunction implements Function, Expression {

    public SingleFunction() {

    }

    @Override
    public void saveParams(LinkedList<Expression> params) {
        if ((params.size() > 1)) {
            throw new BadFormulaException();
        } else if (params.size() == 1) {
            if (params.get(0) != null) {
                throw new BadFormulaException();
            }
        }
    }

    @Override
    public double eval() {
        throw new BadFormulaException();
    }
}
