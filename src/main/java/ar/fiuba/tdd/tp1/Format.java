package ar.fiuba.tdd.tp1;

public class Format {
  
    private String type;
    private String format;
    private String formater;
    private Format child;
  
    Format(String type) {
        this.type = type;
        this.format = null;
        this.formater = null;
        this.child = null;
    }
    
    Format(String type, String format, String formater) {
        this.type = type;
        this.format = format;
        this.formater = formater;
        this.child = null;
    }
  
    public void setChild(Format newChild) {
        child = newChild;
    }
  
    public Format getChild() {
        return child;
    }
    
    public void setFormat(String newFormat) {
        format = newFormat;
    }
    
    public void setFormater(String newFormater) {
        formater = newFormater;
    }
    
    public String getFormat() {
        return format;
    }
    
    public String getFormater() {
        return formater;
    }
    
    public String getType() {
        return type;
    }
  
    public void setType(String type) {
        this.type = type.toLowerCase();
    }

}
