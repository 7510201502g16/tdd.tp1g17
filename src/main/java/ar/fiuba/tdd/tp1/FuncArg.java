package ar.fiuba.tdd.tp1;

import java.util.LinkedList;

public interface FuncArg {

    public abstract LinkedList<Double> exec();
}
