package ar.fiuba.tdd.tp1.acceptance.driver;

import ar.fiuba.tdd.tp1.UndeclaredWorkSheetException;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.List;


public interface SpreadSheetTestDriver {

    List<String> workBooksNames();

    void createNewWorkBookNamed(String name);

    void createNewWorkSheetNamed(String workbookName, String name);

    List<String> workSheetNamesFor(String workBookName);

    void setCellValue(String workBookName, String workSheetName, String cellId, String value)
            throws UndeclaredWorkSheetException;

    String getCellValueAsString(String workBookName, String workSheetName, String cellId)
            throws UndeclaredWorkSheetException;

    double getCellValueAsDouble(String workBookName, String workSheetName, String cellId)
            throws UndeclaredWorkSheetException;

    void undo();

    void redo();

    void setCellFormatter(String workBookName, String workSheetName, String cellId,
            String formatter, String format) throws UndeclaredWorkSheetException;

    void setCellType(String workBookName, String workSheetName, String cellId, String type)
            throws UndeclaredWorkSheetException;

    void persistWorkBook(String workBookName, String fileName) throws IOException, ParseException;

    void reloadPersistedWorkBook(String fileName) throws ParseException, IOException, UndeclaredWorkSheetException;

    void saveAsCSV(String workBookName, String sheetName, String path) throws IOException, UndeclaredWorkSheetException;

    void loadFromCSV(String workBookName, String sheetName, String path) throws IOException, UndeclaredWorkSheetException;

    int sheetCountFor(String workBookName);

    String getCellFormulaAsString(String workBookName, String workSheetName, String cellId) throws UndeclaredWorkSheetException;

    void createNamedRange(String name, String range);
}
