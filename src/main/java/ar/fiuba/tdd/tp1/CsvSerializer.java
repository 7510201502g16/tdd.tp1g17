package ar.fiuba.tdd.tp1;

import java.io.*;
import java.util.ArrayList;

public class CsvSerializer implements Serializer {
    private String updateString(int actualCol, int quantityOfCols, String string) {
        if (actualCol != (quantityOfCols - 1)) {
            return string.concat(";");
        }
        return string;
    }

    private String serializeCells(Spreadsheet sheet, ArrayList<CellComparator> cells, Position position) {
        int index = 0;
        String string = "";
        for (int i = 0; i < sheet.getQuantityOfRows(); i++) {
            for (int j = 0; j < sheet.getQuantityOfCols(); j++) {
                if (((int) position.getFirst() == j) && ((int) position.getSecond() == i)) {
                    string = string.concat(cells.get(index).getCell().getValue());
                    index++;
                    if (index < cells.size()) {
                        position = cells.get(index).getPosition();
                    }
                }
                string = updateString(j, sheet.getQuantityOfCols(), string);
            }
            string = string.concat("\n");
        }
        return string;
    }

    public String serialize(Document document, Spreadsheet sheet, String pathFile) throws IOException {
        ArrayList<CellComparator> cells = sheet.getCellComparators();
        String string = "";
        Position position;
        if (cells.size() > 0) {
            position = cells.get(0).getPosition();
        } else {
            return string;
        }
        string = serializeCells(sheet, cells, position);
        saveToFile(pathFile, string);
        return string;
    }

    public Document deserialize(String docName, String pathFile) throws IOException, UndeclaredWorkSheetException {
        String string = loadFile(pathFile);
        Document document = new Document(docName);
        Spreadsheet sheet = document.getDefaultSheet();
        String[] lines = string.split("\n");
        String[] aux = lines[0].split(";",lines.length);
        int numberOfColumns = aux.length;
        int numberOfRows = lines.length;
        for (int i = 0; i < numberOfRows; i++) {
            String[] values = lines[i].split(";",lines.length);
            for (int j = 0; j < numberOfColumns; j++) {
                if (values[j].compareTo("") != 0) {
                    Cell cell = new Cell();
                    cell.setValue(new StringType(values[j]), values[j]);
                    sheet.addCell(j, i, cell);
                }
            }
        }
        return document;
    }
}
