package ar.fiuba.tdd.tp1;

public class Position<X, Y> {
    private X first;
    private Y second;

    public Position(X first, Y second) {
        this.first = first;
        this.second = second;
    }
  
    public X getFirst() {
        return first;
    }

    public void setFirst(X first) {
        this.first = first;
    }

    public Y getSecond() {
        return second;
    }

    public void setSecond(Y second) {
        this.second = second;
    }

    public boolean equals(Object other) {
        if (other instanceof Position) {
            Position otherPosition = (Position) other;
            return 
                ((  this.first == otherPosition.first 
                ||  ( this.first != null && otherPosition.first != null 
                &&  this.first.equals(otherPosition.first))) 
                &&  (  this.second == otherPosition.second 
                ||  ( this.second != null && otherPosition.second != null 
                &&  this.second.equals(otherPosition.second))) );
        }
        return false;
    }
    
    public int hashCode() {
        int hashFirst = first != null ? first.hashCode() : 0;
        int hashSecond = second != null ? second.hashCode() : 0;

        return (hashFirst + hashSecond) * hashSecond + hashFirst;
    }

    public String toString() { 
        return "(" + first + ", " + second + ")"; 
    }

}
