package ar.fiuba.tdd.tp1;

public class Document implements Creator, Created {
    private CreatedList<Spreadsheet> sheets;
    private CommandHistory commandHistory;
    private String name;

    Document(String newName) {
        Spreadsheet spreadsheet = new Spreadsheet(this, "default");
        sheets = new CreatedList<>();
        sheets.addAtEnd(spreadsheet);
        commandHistory = new CommandHistory();
        name = newName;
    }

    public int getQuantityOfSheets() {
        return sheets.getList().size();
    }

    public void addNewCommand(Command command) {
        commandHistory.addCommandToUndoStack(command);
        commandHistory.clearRedoStack();
    }

    @Override
    public CreatedList getCreatedsList() {
        return sheets;
    }

    @Override
    public void create(String name) {
        if (this.sheets.contains(name)) {
            return;
        }
        Spreadsheet newSpreadsheet = new Spreadsheet(this, name);
        CommandCreate createSpreadsheet = new CommandCreate(this, newSpreadsheet);
        addNewCommand(createSpreadsheet);
        this.sheets.addAtEnd(newSpreadsheet);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String newName) {
        this.name = newName;
    }

    public void redo() {
        if (!commandHistory.getRedoStack().isEmpty()) {
            Command command = (Command) (commandHistory.getRedoStack().pop());
            command.execute();
            commandHistory.addCommandToUndoStack(command);
        }
    }

    public void undo() {
        if (!commandHistory.getUndoStack().isEmpty()) {
            Command command = (Command) (commandHistory.getUndoStack().pop());
            command.undo();
            commandHistory.addCommandToRedoStack(command);
        }
    }

    public Spreadsheet getSpreadsheet(String sheetName) throws UndeclaredWorkSheetException {
        Spreadsheet result = (Spreadsheet) sheets.get(sheetName);
        if (result == null) {
            throw new UndeclaredWorkSheetException();
        }
        return result;
    }

    public Spreadsheet getDefaultSheet() {
        return (Spreadsheet) sheets.get("default");
    }
}