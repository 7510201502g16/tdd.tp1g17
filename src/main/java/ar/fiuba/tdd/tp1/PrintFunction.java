package ar.fiuba.tdd.tp1;

import java.util.LinkedList;

/**
 * Created by maxi on 07/11/15.
 */
public class PrintFunction implements Function, Expression {

    private String result;

    @Override
    public double eval() {
        return 0;
    }

    @Override
    public Function getCopy() {
        return new PrintFunction();
    }

    public String toString() {
        return result;
    }

    @Override
    public void saveParams(LinkedList<Expression> params) {
        String firstParam = params.get(0).toString();
        Character quotationMark = "\"".charAt(0);
        if ((firstParam.charAt(0) != quotationMark) || (firstParam.charAt(firstParam.length() - 1) != quotationMark)) {
            throw new BadFormulaException();
        }
        firstParam = firstParam.substring(1, params.get(0).toString().length() - 1);

        for (int i = 1; i < params.size(); i++) {
            String printIndex = new StringBuilder().append("$").append(i).toString();
            if (firstParam.contains(printIndex)) {
                firstParam = firstParam.replace(printIndex, params.get(i).toString());
            } else {
                throw new BadFormulaException();
            }
        }
        this.result = firstParam;
    }

}
