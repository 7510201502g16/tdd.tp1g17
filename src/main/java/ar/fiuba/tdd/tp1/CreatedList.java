package ar.fiuba.tdd.tp1;

import java.util.ArrayList;

public class CreatedList<T> {
    ArrayList<T> list;

    CreatedList() {
        list = new ArrayList<>();
    }

    public ArrayList<T> getList() {
        return list;
    }

    public ArrayList<T> getStringList() {
        ArrayList newList = new ArrayList<>();
        for (T element:this.list) {
            newList.add(((Created) element).getName());
        }
        return newList;
    }

    public boolean contains(Created created) {
        return list.contains(created);
    }
        
    public boolean contains(String name) {
        Created created;
        for (T element:this.list) {
            created = (Created) element;
            if (created.getName().equals(name)) {
                return true;
            }
        }
        return false;
    }

    public void addAtEnd(Created created) {
        if (!contains(created.getName())) {
            list.add((T) created);
        }
    }

    public void remove(Created created) {
        list.remove(created);
    }

    public boolean isEmpty() {
        return list.isEmpty();
    }

    public Created get(String name) {
        Created created;
        for (T element : this.list) {
            created = (Created) element;
            if (created.getName().equals(name)) {
                return created;
            }
        }
        return null;
    }
}
