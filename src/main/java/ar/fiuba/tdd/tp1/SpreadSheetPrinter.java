package ar.fiuba.tdd.tp1;

public class SpreadSheetPrinter implements Printable {

    private MainSpreadSheet mss;
    int sizeX;
    int sizeY;

    SpreadSheetPrinter(MainSpreadSheet mss) {
        this.mss = mss;
        this.sizeX = 10;
        this.sizeY = 10;
    }

    @Override
    public void print() {
        System.out.println("Document: " + mss.getActualDoc().getName());
        System.out.println("SpreadSheet: " + mss.getCurrentSheet().getName());
        for (int i = 0; i < sizeX; i++) {
            for (int j = 0; j < sizeY; j++) {
                Cell cell = mss.getCellFromCurrentSheet(j, i);
                System.out.print(cell.getName() + ": ");
                System.out.print(cell.getStringValue() + "\t\t");
            }
            System.out.print("\n");
        }
    }

}
