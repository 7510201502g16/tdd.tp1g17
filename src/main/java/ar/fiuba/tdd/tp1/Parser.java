package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.Tokenizer.Symbol;

public class Parser {

    protected Tokenizer tokenizer;
    protected MainSpreadSheet fss;

    Parser() {
        this.tokenizer = new Tokenizer();
        this.fss = null;
    }
    
    Parser(Parser other) {
        this.tokenizer = other.tokenizer;
        this.fss = other.fss;
    }

 
    
    public void setMSS(MainSpreadSheet mainSpreadSheet) {
        this.fss = mainSpreadSheet;
    }

    public int[] parseCellPos(String src) {
        int[] res = new int[2];
        Tokenizer tokenizer = new Tokenizer();
        tokenizer.setSrc(src);
        String result = tokenizer.getMatch(Symbol.CHARSEQ);
        res[0] = parseCellColumn(result);
        res[1] = Integer.parseInt(tokenizer.getMatch(Symbol.NUMBER)) - 1;
        return res;
    }

    private int parseCellColumn(String src) {
        int res = 0;
        src = src.toLowerCase();
        int base = 'a';
        int mod = 'z' - 'a' + 1;
        int pow = 0;
        for (int i = src.length() - 1; i >= 0; i--) {
            int val = src.charAt(i) - base + 1;
            res = (int) (res + val * Math.pow(mod, pow));
            pow++;
        }
        return res - 1;
    }
}
