package ar.fiuba.tdd.tp1;

import java.security.PublicKey;
import java.util.LinkedList;

/**
 * Created by matias on 11/5/15.
 */
public abstract class SubstringFunction implements Function, Expression {

    protected Expression firstArgument;
    protected Expression secondArgument;
    protected LinkedList<Expression> params;

    public SubstringFunction() {
        this.firstArgument = null;
        this.secondArgument = null;
    }

    public SubstringFunction(LinkedList<Expression> values) {
        saveParams(values);
    }

    protected Integer castSecondArgument(String argument) {
        try {
            Integer value = Double.valueOf(argument).intValue();
            return value;
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public void saveParams(LinkedList<Expression> params) {
        this.params = params;
        if (this.params != null) {
            this.firstArgument = this.params.removeFirst();
            this.secondArgument = this.params.removeFirst();
        }
    }

    @Override
    public double eval() {
        throw new BadFormulaException();
    }
}
