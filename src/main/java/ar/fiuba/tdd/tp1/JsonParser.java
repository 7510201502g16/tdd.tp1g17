package ar.fiuba.tdd.tp1;

import org.json.simple.parser.ContainerFactory;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.*;

public class JsonParser {
    private Map json;
  
    public Map parse(String jsonText) {
        JSONParser parser = new JSONParser();
        ContainerFactory containerFactory = new ContainerFactory(){
            public List creatArrayContainer() {
                return new LinkedList();
            }

            public Map createObjectContainer() {
                return new LinkedHashMap();
            }
        };
        try {
            json = (Map)parser.parse(jsonText, containerFactory);
        } catch (ParseException pe) {
            System.out.println(pe);
        }
        return json;
    }
}
