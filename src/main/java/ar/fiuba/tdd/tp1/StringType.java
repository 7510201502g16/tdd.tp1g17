package ar.fiuba.tdd.tp1;

public class StringType implements Expression, Formatter {
    private String string;

    public StringType(String string) {
        this.string = string;
    }

    @Override
    public double eval() {
        try {
            return Double.parseDouble(string);
        } catch (Exception e) {
            string = "It is not a number.";
        }
        throw new BadFormulaException();
    }

    @Override
    public void setFormatter(String formatter, String format) {
    }

    public String toString() {
        return string;
    }
}