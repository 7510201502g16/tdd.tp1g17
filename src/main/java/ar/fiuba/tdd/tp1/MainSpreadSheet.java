package ar.fiuba.tdd.tp1;

import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class MainSpreadSheet {
    private SpreadsheetEditor spreadsheetEditor;
    private Document actualDoc;
    private Spreadsheet actualSheet;
    private JsonSerializer json;
    private CsvSerializer csv;
    private HashMap<String, String> namedRanges;

    public MainSpreadSheet() {
        this.spreadsheetEditor = new SpreadsheetEditor();
        this.actualDoc = null;
        this.actualSheet = null;
        json = new JsonSerializer();
        csv = new CsvSerializer();
        namedRanges = new HashMap<>();
    }

    public SpreadsheetEditor getSpreadsheetEditor() {
        return spreadsheetEditor;
    }

    public void setActualDoc(Document doc) {
        actualDoc = doc;
    }

    public void setActualSheet(Spreadsheet sheet) {
        actualSheet = sheet;
    }

    public Document getActualDoc() {
        return actualDoc;
    }

    public void createDocument(String docName) {
        this.spreadsheetEditor.create(docName);
        actualDoc = spreadsheetEditor.getDocument(docName);
        actualSheet = actualDoc.getDefaultSheet();
    }

    public void createSheet(String docName, String sheetName) {
        this.actualDoc = spreadsheetEditor.getDocument(docName);
        createSheet(sheetName);
    }

    public void createSheet(String sheetName) {
        this.actualDoc.create(sheetName);
        try {
            this.actualSheet = actualDoc.getSpreadsheet(sheetName);
        } catch (UndeclaredWorkSheetException e) {
            return;
        }
    }

    public void createRange(String name, String range) {
        namedRanges.put("R_" + name, range);
        System.out.println("Range" + name + " created, now can be referenced by R_" + name);
    }

    public void createCell(int posX, int posY, String someValue)
            throws UndeclaredWorkSheetException {
        SpreadSheetParser parser = new SpreadSheetParser();
        someValue = replaceNamedRanges(someValue);
        parser.setMSS(this);
        Expression value = null;
        String strValue = someValue;
        value = parser.parseSrc(someValue);
        Cell cell = new Cell();
        cell.setValue(value, strValue);
        actualSheet.addCell(posX, posY, cell);
    }

    private String replaceNamedRanges(String someValue) {
        final Set<Map.Entry<String, String>> entries = namedRanges.entrySet();
        for (Map.Entry<String, String> entry : entries) {
            someValue = someValue.replaceAll(entry.getKey(), entry.getValue());
        }
        return someValue;
    }

    public Cell getCell(String docName, String pageName, int posX, int posY)
            throws UndeclaredWorkSheetException {
        Spreadsheet sheet = getSheet(docName, pageName);
        return sheet.getCell(posX, posY);
    }

    public void changeSpreadSheet(String sheetName) throws UndeclaredWorkSheetException {
        actualSheet = actualDoc.getSpreadsheet(sheetName);
    }

    public String getCellValue(int posX, int posY) {
        Cell cell = getCellFromCurrentSheet(posX, posY);
        Double result = cell.eval();
        return result.toString();
    }

    public Spreadsheet getCurrentSheet() {
        return actualSheet;
    }

    public Cell getCellFromCurrentSheet(int posX, int posY) {
        return actualSheet.getCell(posX, posY);
    }

    public Spreadsheet getSheet(String docName, String pageName) throws UndeclaredWorkSheetException {
        return spreadsheetEditor.getDocument(docName).getSpreadsheet(pageName);
    }

    public String serialize(String fileName) throws IOException, ParseException {
        return json.serialize(actualDoc, fileName);
    }

    public String serializeCSV(String pathFile) throws IOException {
        return csv.serialize(actualDoc, actualSheet, pathFile);
    }

    public void deserializeCSV(String docName, String pathFile) throws IOException, UndeclaredWorkSheetException {
        setDocument(csv.deserialize(docName, pathFile));
    }

    public void setDocument(Document doc) {
        spreadsheetEditor.deleteDocument(doc);
        spreadsheetEditor.setDocument(doc);
        actualDoc = spreadsheetEditor.getDocument(doc.getName());
        actualSheet = actualDoc.getDefaultSheet();
    }

    public void deserialize(String pathFile) throws IOException, ParseException, UndeclaredWorkSheetException {
        Document doc = json.deserialize(pathFile);
        spreadsheetEditor.deleteDocument(doc);
        setDocument(doc);
    }

    public void redo() {
        this.actualDoc.redo();
    }

    public void undo() {
        this.actualDoc.undo();
    }

    public void changeDoc(String docName) {
        this.actualDoc = spreadsheetEditor.getDocument(docName);
    }

    public void deleteDocuments() {
        spreadsheetEditor.getCreatedsList().getList().clear();
    }

    public String getCellFormulaAsString(String workBookName, String workSheetName, int posX,
                                         int posY) throws UndeclaredWorkSheetException {
        Cell cell = getCell(workBookName, workSheetName, posX, posY);
        return cell.getSrc();
    }
}
