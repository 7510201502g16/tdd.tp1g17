package ar.fiuba.tdd.tp1;

public interface Command {
    void execute();

    void undo();
}