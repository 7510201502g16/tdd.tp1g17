package ar.fiuba.tdd.tp1;

import java.util.LinkedList;

/**
 * Created by matias on 11/5/15.
 */
public class RightFunction extends SubstringFunction {

    public RightFunction() {
        super();
    }

    public RightFunction(LinkedList<Expression> values) {
        super(values);
    }

    @Override
    public Function getCopy() {
        return new RightFunction(this.params);
    }


    public String toString() {
        Integer amount = this.castSecondArgument(this.secondArgument.toString());
        return this.firstArgument.toString().substring(this.firstArgument.toString().length() - amount);
    }
}
