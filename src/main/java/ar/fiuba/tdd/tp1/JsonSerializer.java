package ar.fiuba.tdd.tp1;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

public class JsonSerializer implements Serializer {
    private JSONObject obj;
    private JsonParser parser;

    JsonSerializer() {
        obj = new JSONObject();
        parser = new JsonParser();
    }

    private void parseFormatter(JSONParser pp, Map mapCell, Cell cell) throws ParseException {
        JSONArray formatter = (JSONArray) pp.parse(JSONValue.toJSONString(mapCell.get("formatter")));
        int index = 0;
        while (index < formatter.size()) {
            Map mapFormatter = parser.parse(formatter.get(index).toString());
            Iterator it = mapFormatter.keySet().iterator();
            while (it.hasNext()) {
                String key = it.next().toString();
                cell.setFormatter(key,mapFormatter.get(key).toString());
            }
            index++;
        }
    }
    
    private void deserializeCells(Document doc, Parser ep, Map map) throws ParseException, UndeclaredWorkSheetException {
        JSONParser pp = new JSONParser();
        JSONArray array = (JSONArray) pp.parse(JSONValue.toJSONString(map.get("cells")));
        for (int index = 0 ; index < array.size() ; index ++) {
            Map mapCell = parser.parse(array.get(index).toString());
            String sheetName = mapCell.get("sheet").toString();
            doc.create(sheetName);
            Cell cell = new Cell();
            cell.setSrc(mapCell.get("string_value").toString());
            cell.setValue(new StringType(mapCell.get("value").toString()), mapCell.get("value").toString());
            cell.checkType(mapCell.get("type").toString().toLowerCase());
            parseFormatter(pp, mapCell, cell);
            Spreadsheet sp = doc.getSpreadsheet(sheetName);
            int[] pos = ep.parseCellPos(mapCell.get("id").toString());
            sp.addCell(pos[0],pos[1], cell);
        }
    } 

    public Document deserialize(String pathFile) throws IOException, ParseException, UndeclaredWorkSheetException {
        String st = loadFile(pathFile);
        Map map = parser.parse(st);
        Parser ep = new Parser();
        Document doc = new Document(map.get("name").toString());
        deserializeCells(doc, ep, map);
        return doc;
    }

    private void serializeCell(Spreadsheet sp, JSONArray list1, int index) {
        Cell cell = sp.getCellComparators().get(index).getCell();
        JSONObject jsonCell = new JSONObject();
        jsonCell.put("sheet", sp.getName());
        jsonCell.put("id", cell.getName());
        jsonCell.put("value", cell.getValue());
        jsonCell.put("string_value", cell.getSrc());
        jsonCell.put("type", cell.getFormat().getType());
        JSONObject jsonFormatter = new JSONObject();
        Format format = cell.getFormat();
        while (format != null) {
            jsonFormatter.put(format.getFormater(), format.getFormat());
            format = format.getChild();
        }
        JSONArray listFormatter = new JSONArray();
        listFormatter.add(jsonFormatter);
        jsonCell.put("formatter", listFormatter);
        list1.add(jsonCell);
    }

    public String serialize(Document document, String fileName) throws IOException {
        obj.put("name", document.getName());
        JSONArray list1 = new JSONArray();
        for (int index = 0; index < document.getCreatedsList().getList().size(); index++) {
            Spreadsheet sp = (Spreadsheet) document.getCreatedsList().getList().get(index);
            for (int index2 = 0; index2 < sp.getCellComparators().size(); index2++) {
                serializeCell(sp, list1, index2);
            }
        }
        obj.put("cells", list1);
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String jsonOutput = gson.toJson(obj);
        saveToFile(fileName, jsonOutput);
        return jsonOutput;
    }
}
