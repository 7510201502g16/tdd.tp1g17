package ar.fiuba.tdd.tp1;

public class CommandCreate<T, V> implements Command{
    private T creator;
    private V created;

    CommandCreate(T someCreator, V someCreated) {
        creator = someCreator;
        created = someCreated;
    }

    public void execute() {
        if (!(((Creator) creator).getCreatedsList().contains((Created) created))) {
            ((Creator) creator).getCreatedsList().addAtEnd((Created) created);
        }
    }

    public void undo() {
        ((Creator) creator).getCreatedsList().remove((Created) created);
    }
}