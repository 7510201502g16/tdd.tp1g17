package ar.fiuba.tdd.tp1;

public class BinaryOperatorExpression extends MathExpression {
    private BinaryOperatorExpression(Expression left, Expression right, BinaryOperator op) {
        this.left = left;
        this.right = right;
        this.op = op;
    }

    private Expression left;
    private Expression right;
    private BinaryOperator op;

    @Override
    public double eval() {
        double resultLeft = left.eval();
        double resultRight = right.eval();
        return op.operate(resultLeft, resultRight);
    }

    public static BinaryOperatorExpression add(Expression left, Expression right) {
        return new BinaryOperatorExpression(left,right,(double number1, double number2) -> number1 + number2);
    }

    public static BinaryOperatorExpression sub(Expression left, Expression right) {
        return new BinaryOperatorExpression(left,right,(double number1, double number2) -> number1 - number2);
    }

    public static BinaryOperatorExpression star(Expression left, Expression right) {
        return new BinaryOperatorExpression(left,right,(double number1, double number2) -> number1 * number2);
    }

    public static BinaryOperatorExpression div(Expression left, Expression right) {
        return new BinaryOperatorExpression(left,right,(double number1, double number2) -> number1 / number2);
    }

    public static BinaryOperatorExpression pow(Expression left, Expression right) {
        return new BinaryOperatorExpression(left,right,(double number1, double number2) -> Math.pow(number1,number2));
    }
}