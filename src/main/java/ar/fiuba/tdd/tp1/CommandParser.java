package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.Tokenizer.Symbol;

import java.util.HashMap;

public class CommandParser extends Parser {

    private HashMap<String, Command> commands;

    CommandParser(MainSpreadSheet mss) {
        this.fss = mss;
        this.commands = new HashMap<String, Command>();
        loadCommands();
    }

    private void loadCommands() {
        loadCreate();
        loadChange();
        loadPut();
        loadUndo();
        loadRedo();
        loadExit();
        loadLoadFromFile();
        loadSaveToFile();
        loadFormat();
        loadGetCell();
        loadNamedRange();
    }

    private void loadNamedRange() {
        commands.put("NAMED_RANGE", (args) -> {
                try {
                    fss.createRange(args[0], args[1]);
                } catch (ArrayIndexOutOfBoundsException e) {
                    return true;
                }
                return true;
            });
    }

    private void loadGetCell() {
        commands.put("GET_CELL", (args) -> {
                int[] pos = parseCellPos(args[0]);
                System.out.println(fss.getCellFromCurrentSheet(pos[0], pos[1]).getStringValue());
                return true;
            });
    }

    private void loadFormat() {
        commands.put("SET_TYPE", (args) -> {
                int[] pos = parseCellPos(args[0]);
                String type = args[1];
                String formatter = args[2];
                String format = args[3];
                fss.getCellFromCurrentSheet(pos[0], pos[1]).setType(type, formatter, format);
                return true;
            });
    }


    private void loadSaveToFile() {
        commands.put("SAVE_AS_JSON", (args) -> {
                try {
                    fss.serialize(args[0]);
                    return true;
                } catch (Exception e) {
                    System.out.println("Error al escribir el archivo: " + args[0]);
                    return false;
                }
            });
        commands.put("SAVE_AS_CSV", (args) -> {
                try {
                    fss.serializeCSV(args[0]);
                    return true;
                } catch (Exception e) {
                    System.out.println("Error al escribir el archivo: " + args[0]);
                    return false;
                }
            });
    }

    private void loadLoadFromFile() {
        commands.put("LOAD_FROM_JSON", (args) -> {
                try {
                    fss.deserialize(args[0]);
                } catch (Exception e) {
                    System.out.println("Error cargando archivo: " + args[0]);
                    return false;
                }
                return true;
            });
        commands.put("LOAD_FROM_CSV", (args) -> {
                try {
                    fss.deserializeCSV(fss.getActualDoc().getName(), args[0]);
                } catch (Exception e) {
                    System.out.println("Error al cargar: " + args[0]);
                    return false;
                }
                return true;
            });
    }

    private void loadCreate() {
        commands.put("CREATE_DOC", new CommandCreate());
        commands.put("CREATE_SHEET", (args) -> {
                fss.createSheet(args[0]);
                return true;
            });

    }

    private void loadPut() {
        commands.put("PUT", (args) -> {
                int[] pos = parseCellPos(args[0]);
                try {
                    fss.createCell(pos[0], pos[1], args[1]);
                } catch (Exception e) {
                    fss.getActualDoc();
                }
                return true;
            });
    }

    private void loadChange() {
        commands.put("CHANGE_SHEET", (args) -> {
                Spreadsheet old = fss.getCurrentSheet();
                try {
                    fss.changeSpreadSheet(args[0]);
                } catch (UndeclaredWorkSheetException e) {
                    fss.setActualSheet(old);
                }
                return true;
            });
        commands.put("CHANGE_DOC", (args) -> {
                fss.changeDoc(args[0]);
                return true;
            });
    }


    private void loadUndo() {
        commands.put("UNDO", (args) -> {
                fss.undo();
                return true;
            });
    }

    private void loadRedo() {
        commands.put("REDO", (args) -> {
                fss.redo();
                return true;
            });
    }

    private void loadExit() {
        commands.put("EXIT", (args) -> {
                return false;
            });
    }

    public boolean parseSrc(String src) {
        tokenizer.setSrc(src);
        String commName = tokenizer.getMatch(Symbol.COMM_NAME);
        String[] args = tokenizer.getRemainingSrc().split("[ ]");
        if (!commands.containsKey(commName)) {
            return false;
        }
        Command command = commands.get(commName);
        return command.exec(args);
    }

    private interface Command {

        public abstract boolean exec(String[] args);

    }

    private class CommandCreate implements Command {

        @Override
        public boolean exec(String[] args) {
            fss.createDocument(args[0]);
            return true;
        }

    }

}
