package ar.fiuba.tdd.tp1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class NumberTest {

    private Number number;

    @Test
         public void numberConstructorWithStringWellInitialized() throws Exception {
        number = new Number("10");
        assertEquals(number.toString(), "10.0");
    }

    @Test
    public void numberConstructorWithStringAndDecimalsWellInitialized() throws Exception {
        number = new Number("10", 5);
        assertEquals(number.toString(), "10.00000");
    }

    @Test
    public void numberConstructorWithDoubleWellInitialized() throws Exception {
        number = new Number(10.0);
        assertEquals(number.toString(), "10.0");
    }

    @Test
    public void numberConstructorWithDoubleAndDecimalsWellInitialized() throws Exception {
        number = new Number(10.0, 5);
        assertEquals(number.toString(), "10.00000");
    }

    @Test
    public void numberWellCutWithLessNumberOfDecimals() throws Exception {
        number = new Number(10.014, 2);
        assertEquals(number.toString(), "10.01");
    }

    @Test
    public void numberWellRoundedWithLessNumberOfDecimals() throws Exception {
        number = new Number(10.015, 2);
        assertEquals(number.toString(), "10.02");
    }
}