package ar.fiuba.tdd.tp1.group16;

import ar.fiuba.tdd.tp1.BadFormulaException;
import ar.fiuba.tdd.tp1.SpreadsheetTestDriverImplementation;
import ar.fiuba.tdd.tp1.UndeclaredWorkSheetException;
import ar.fiuba.tdd.tp1.acceptance.driver.SpreadSheetTestDriver;
import org.junit.Before;
import org.junit.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.assertEquals;

/**
 * Created by matias on 11/1/15.
 */
public class IterationThreeTests {

    private static final double DELTA = 0.0001;
    private SpreadSheetTestDriver testDriver;

    @Before
    public void setUp() {
        testDriver = new SpreadsheetTestDriverImplementation();
        testDriver.createNewWorkBookNamed("tecnicas");
    }

    @Test
    public void powerSimpleTest() throws UndeclaredWorkSheetException {

        testDriver.setCellValue("tecnicas", "default", "C3", "= 3 ^ 3");
        assertEquals(27, testDriver.getCellValueAsDouble("tecnicas", "default", "C3"), DELTA);
    }

    @Test
    public void powerCombinedTest() throws UndeclaredWorkSheetException {
        testDriver.setCellValue("tecnicas", "default", "A1", "3");
        testDriver.setCellValue("tecnicas", "default", "B2", "2");
        testDriver.setCellValue("tecnicas", "default", "C3", "= A1 + B2 ^ A1");
        assertEquals(11, testDriver.getCellValueAsDouble("tecnicas", "default", "C3"), DELTA);
    }

    @Test
    public void leftFunctionTest() throws UndeclaredWorkSheetException {
        testDriver.setCellValue("tecnicas", "default", "A1", "hola");
        testDriver.setCellValue("tecnicas", "default", "C3", "= LEFT(A1,3)");
        assertEquals("hol", testDriver.getCellValueAsString("tecnicas", "default", "C3"));
    }

    @Test
    public void leftFunctionTwoReferenceTest() throws UndeclaredWorkSheetException {
        testDriver.setCellValue("tecnicas", "default", "A1", "hola");
        testDriver.setCellValue("tecnicas", "default", "A2", "3");
        testDriver.setCellValue("tecnicas", "default", "C3", "= LEFT(A1,A2)");
        assertEquals("hol", testDriver.getCellValueAsString("tecnicas", "default", "C3"));
    }

    @Test
    public void righttFunctionTest() throws UndeclaredWorkSheetException {
        testDriver.setCellValue("tecnicas", "default", "A1", "hola");
        testDriver.setCellValue("tecnicas", "default", "C3", "= RIGHT(A1,3)");
        assertEquals("ola", testDriver.getCellValueAsString("tecnicas", "default", "C3"));
    }

    @Test
    public void todayFunctionTest() throws UndeclaredWorkSheetException {
        testDriver.setCellValue("tecnicas", "default", "A1", "= TODAY()");

        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("YYYY-MM-dd");
        assertEquals(dateFormat.format(date), testDriver.getCellValueAsString("tecnicas", "default", "A1"));
    }

    @Test(expected = BadFormulaException.class)
    public void todayFunctionWithOneArgumentTest() throws UndeclaredWorkSheetException {
        testDriver.setCellValue("tecnicas", "default", "A1", "= TODAY(1)");
    }

    @Test
    public void todayFunctionChangingFormatTest() throws UndeclaredWorkSheetException {
        testDriver.setCellValue("tecnicas", "default", "A1", "= TODAY()");
        testDriver.setCellType("tecnicas", "default", "A1", "Date");
        testDriver.setCellFormatter("tecnicas", "default", "A1", "format", "DD-MM-YYYY");

        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-YYYY");
        assertEquals(dateFormat.format(date), testDriver.getCellValueAsString("tecnicas", "default", "A1"));
    }

    @Test
    public void printfFunctionTest() throws UndeclaredWorkSheetException {
        testDriver.setCellValue("tecnicas", "default", "A1", "Juan");
        testDriver.setCellValue("tecnicas", "default", "A2", "Perez");
        testDriver.setCellValue("tecnicas", "default", "A3", "= PRINTF(\"Hola $1 $2\", A1, A2)");

        assertEquals("Hola Juan Perez", testDriver.getCellValueAsString("tecnicas", "default", "A3"));
    }

    @Test(expected = BadFormulaException.class)
    public void printfFunctionWithoutStringTest() throws UndeclaredWorkSheetException {
        testDriver.setCellValue("tecnicas", "default", "A1", "Juan");
        testDriver.setCellValue("tecnicas", "default", "A2", "Perez");
        testDriver.setCellValue("tecnicas", "default", "A3", "= PRINTF(103, A1, A2)");
    }


    @Test
    public void createNamedRange() throws UndeclaredWorkSheetException {
        testDriver.createNamedRange("RANGOPRUEBA", "B1:D1");
        testDriver.setCellValue("tecnicas", "default", "B1", "1");
        testDriver.setCellValue("tecnicas", "default", "C1", "2");
        testDriver.setCellValue("tecnicas", "default", "D1", "3");
        testDriver.setCellValue("tecnicas", "default", "A1", "=SUMA(R_RANGOPRUEBA)");
        final String cellValue = testDriver.getCellValueAsString("tecnicas", "default", "A1");
        assertEquals("6.0", cellValue);
    }

}
