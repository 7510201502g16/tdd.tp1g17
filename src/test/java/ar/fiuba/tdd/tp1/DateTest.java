package ar.fiuba.tdd.tp1;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DateTest {
    private Date date;

    @Before
    public void initialization() throws Exception {
        date = new Date(10, 12, 1970, "10-12-1970");
    }

    @Test
    public void dateWellInitialized() {
        assertEquals(date.getDayAsString(), "10");
        assertEquals(date.getMonthAsString(), "12");
        assertEquals(date.getYearAsString(), "1970");
    }

    @Test (expected = Exception.class)
    public void dateDayOutOfSuperiorRangeFebruaryLeapYear() throws Exception {
        new Date(30, 2, 2000, "30-02-2000");
    }

    @Test (expected = Exception.class)
         public void dateDayOutOfSuperiorRangeFebruaryNotLeapYear() throws Exception {
        new Date(29, 2, 2001, "29-02-2001");
    }

    @Test (expected = Exception.class)
    public void dateDayOutOfInferiorRange() throws Exception {
        new Date(0, 2, 2001, "00-02-2001");
    }

    @Test (expected = Exception.class)
    public void dateMonthOutOfInferiorRange() throws Exception {
        new Date(1, 0, 2001, "01-00-2001");
    }

    @Test (expected = Exception.class)
    public void dateMonthOutOfSuperiorRange() throws Exception {
        new Date(1, 13, 2001, "01-13-2001");
    }

    @Test (expected = Exception.class)
         public void dateYearOutOfInferiorRange() throws Exception {
        new Date(1, 12, -1, "01-12--1");
    }

    @Test (expected = Exception.class)
    public void dateYearOutOfSuperiorRange() throws Exception {
        new Date(1, 12, 10000, "01-12-10000");
    }
}