package ar.fiuba.tdd.tp1;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MoneyTest {
    private Money money;

    @Before
    public void initialization() {
        money = new Money("$10.12", 2);
    }

    @Test
    public void moneyConstructorWithoutDecimalsWellInitialized() throws Exception {
        money = new Money("$10.12");
        assertEquals(money.toString(), "$ 10");
        assertEquals(money.eval(), 10, 0);
    }

    @Test
    public void moneyConstructorWithZeroDecimalsWellInitialized() throws Exception {
        money = new Money("$10.12", 0);
        assertEquals(money.toString(), "$ 10");
        assertEquals(money.eval(), 10, 0);
    }

    @Test
    public void moneyConstructorWithTwoDecimalsWellInitialized() throws Exception {
        money = new Money("$10.12", 2);
        assertEquals(money.toString(), "$ 10.12");
        assertEquals(money.eval(), 10.12, 0);
    }

    @Test
    public void moneyConstructorWithWrongNumberOfDecimalsWellInitialized() throws Exception {
        money = new Money("$10.12", 15);
        assertEquals(money.toString(), "$ 10");
        assertEquals(money.eval(), 10, 0);
    }

    @Test
    public void moneyWithMoreThanTwoDecimalsWellRoundedToTwo() throws Exception {
        money = new Money("$10.125", 2);
        assertEquals(money.toString(), "$ 10.13");
        assertEquals(money.eval(), 10.13, 0);
    }

    @Test
    public void multiplicationBetweenMoneyAndNumber() throws Exception {
        Number two = new Number(2);
        BinaryOperatorExpression firstMultiplication = BinaryOperatorExpression.star(money, two);
        BinaryOperatorExpression secondMultiplication = BinaryOperatorExpression.star(two, money);
        assertEquals(firstMultiplication.eval(), 20.24, 0);
        assertEquals(secondMultiplication.eval(), 20.24, 0);
        assertEquals(firstMultiplication.eval(), secondMultiplication.eval(), 0);
    }

    @Test
    public void setNewSymbol() throws Exception {
        String symbol = "U$S";
        money.setSymbol(symbol);
        assertEquals(money.toString(), "U$S 10.12");
    }

    @Test
    public void setNewValue() throws Exception {
        Number two = new Number(2);
        money.setValue(two);
        assertEquals(money.toString(), "$ 2.00");
    }

    @Test
    public void setNewQuantityOfDecimalsZero() throws Exception {
        int zero = 0;
        money.setQuantityOfDecimals(zero);
        assertEquals(money.toString(), "$ 10");
    }

    @Test
    public void setNewQuantityOfDecimalsTwo() throws Exception {
        money = new Money("$5.125", 0);
        int two = 2;
        money.setQuantityOfDecimals(two);
        assertEquals(money.toString(), "$ 5.00");
    }

    @Test
    public void setNewQuantityOfDecimalsInvalid() throws Exception {
        int three = 3;
        money.setQuantityOfDecimals(three);
        assertEquals(money.toString(), "$ 10.12");
    }
}