package ar.fiuba.tdd.tp1;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class CommandCreateTest {
    private CommandCreate createSheet;
    private CommandCreate createDocument;
    private Document document;
    private Spreadsheet sheet;
    private SpreadsheetEditor editor;

    @Before
    public void initialization() {
        document = new Document("nuevoDocumento");
        sheet = new Spreadsheet(document, "nuevaHoja");
        editor = new SpreadsheetEditor();
        createSheet = new CommandCreate(document, sheet);
        createDocument = new CommandCreate(editor, document);
    }

    @Test
    public void executeForCreateDocument() throws Exception {
        createDocument.execute();
        assertTrue(editor.getCreatedsList().contains(document));
    }

    @Test
    public void undoForCreateDocument() throws Exception {
        createDocument.undo();
        assertTrue(editor.getCreatedsList().isEmpty());
    }

    @Test
    public void executeForCreateSheet() throws Exception {
        createSheet.execute();
        assertTrue(document.getCreatedsList().contains(sheet));
    }

    @Test
    public void undoForCreateSheet() throws Exception {
        createSheet.undo();
        assertTrue(document.getCreatedsList().contains("default"));
    }
}