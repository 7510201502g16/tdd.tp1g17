package ar.fiuba.tdd.tp1;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class WriteCellTest {
    private Cell cell;
    private Cell newCell;
    private WriteCell writeCell;
    private Number five;

    @Before
    public void initialization() {
        cell = new Cell();
        cell.setName("A1");
        five = new Number(5);
        newCell = new Cell();
        newCell.setName(cell.getName());
        newCell.setValue(five, five.toString());
        writeCell = new WriteCell(cell, newCell);
    }

    @Test
    public void execute() throws Exception {
        writeCell.execute();
        assertEquals(cell.getSrc(),"5.0");
    }

    @Test
    public void undo() throws Exception {
        writeCell.undo();
        assertEquals(cell.getSrc(),"");
    }

    @Test
    public void undoAfterExecute() throws Exception {
        writeCell.execute();
        writeCell.undo();
        assertEquals(cell.getSrc(),"");
    }

    @Test
    public void executeAfterUndo() throws Exception {
        writeCell.undo();
        writeCell.execute();
        assertEquals(cell.getSrc(),"5.0");
    }

    @Test
    public void executeTwice() throws Exception {
        writeCell.execute();
        writeCell.execute();
        assertEquals(cell.getSrc(),"5.0");
    }

    @Test
    public void undoTwice() throws Exception {
        writeCell.undo();
        writeCell.undo();
        assertEquals(cell.getSrc(),"");
    }

    @Test
         public void executeTwiceAndUndoOnce() throws Exception {
        writeCell.execute();
        writeCell.execute();
        writeCell.undo();
        assertEquals(cell.getSrc(),"");
    }

    @Test
    public void undoTwiceAndExecuteOnce() throws Exception {
        writeCell.undo();
        writeCell.undo();
        writeCell.execute();
        assertEquals(cell.getSrc(),"5.0");
    }

    @Test
    public void executeUndoExecute() throws Exception {
        writeCell.execute();
        writeCell.undo();
        writeCell.execute();
        assertEquals(cell.getSrc(),"5.0");
    }

    @Test
    public void undoExecuteUndo() throws Exception {
        writeCell.undo();
        writeCell.execute();
        writeCell.undo();
        assertEquals(cell.getSrc(),"");
    }

    @Test
    public void executeChangingCellValueKeepsTheSame() throws Exception {
        Number twenty = new Number(20);
        writeCell.execute();
        cell.setValue(twenty, twenty.toString());
        writeCell.execute();
        assertEquals(cell.getSrc(),"5.0");
    }

    @Test
    public void undoChangingCellValueKeepsTheSame() throws Exception {
        Number twenty = new Number(20);
        writeCell.execute();
        cell.setValue(twenty, twenty.toString());
        writeCell.execute();
        writeCell.undo();
        assertEquals(cell.getSrc(),"");
    }
}