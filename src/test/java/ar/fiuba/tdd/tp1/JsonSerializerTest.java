package ar.fiuba.tdd.tp1;

import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.Scanner;

import static org.junit.Assert.assertEquals;

public class JsonSerializerTest {
    private MainSpreadSheet mss;
    private String docName;
    private String serializeContent;
    private String fileContent;

    @Before
    public void initialization() {
        mss = new MainSpreadSheet();
        docName = "doc1.txt";
        mss.createDocument(docName);
    }

    @Test
    public void serializeDocWithACell() throws Exception {
        mss.createSheet(docName, "hoja1");
        mss.createCell(1, 1, "2010-10-10");
        serializeContent = mss.serialize(docName);
        fileContent = new Scanner(new File(docName)).useDelimiter("\\Z").next();
        assertEquals(serializeContent, fileContent);
    }
   
    @Test
    public void serializeDoc()  throws Exception {
        serializeContent = mss.serialize(docName);
        fileContent = new Scanner(new File(docName)).useDelimiter("\\Z").next();
        assertEquals(serializeContent, fileContent);
    }
    
    @Test
    public void serializeDocWithTypeNumber()  throws Exception {
        mss.createDocument("doc3");
        mss.createSheet("doc3", "hoja1");
        mss.createCell(1, 1, "10");
        mss.getCell("doc3", "hoja1", 1, 1).setType("number","decimal","2");
        serializeContent = mss.serialize(docName);
        fileContent = new Scanner(new File("doc3" + ".txt")).useDelimiter("\\Z").next();
        assertEquals(serializeContent, fileContent);
    }
    
    @Test
    public void serializeDocWithTwoCells()  throws Exception {
        mss.createDocument("doc4");
        mss.createSheet("doc4", "hoja1");
        mss.createCell(1, 2, "10");
        mss.createCell(1, 1, "10");
        serializeContent = mss.serialize(docName);
        fileContent = new Scanner(new File("doc4" + ".txt")).useDelimiter("\\Z").next();
        assertEquals(serializeContent, fileContent);
    }
    
    @Test
    public void serializeDocWithMoney()  throws Exception {
        mss.createDocument("doc5");
        mss.createSheet("doc5", "hoja1");
        mss.createCell(1, 1, "10");
        mss.getCell("doc5", "hoja1", 1, 1).setType("currency", "decimal","2");
        mss.getCell("doc5", "hoja1", 1, 1).setType("currency", "symbol","U$S");
        serializeContent = mss.serialize(docName);
        fileContent = new Scanner(new File("doc5" + ".txt")).useDelimiter("\\Z").next();
        assertEquals(serializeContent, fileContent);
    }
}
