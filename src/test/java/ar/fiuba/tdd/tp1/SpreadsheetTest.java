package ar.fiuba.tdd.tp1;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SpreadsheetTest {
    private String pageName;
    private String newName;
    private String cellName;
    private Spreadsheet spreadsheet;
    private Document document;
    private Cell cell;

    @Before
    public void initialization() {
        document = new Document("Document 1");
        pageName = "hoja1";
        cellName = "A1";
        spreadsheet = new Spreadsheet(document, pageName);
        cell = new Cell();
        cell.setName(cellName);
        spreadsheet.addCell(1, 1, cell);
        newName = "nuevoNombre";
        spreadsheet.setName(newName);
    }

    @Test
    public void valueAddedCorrectly() {
        assertEquals(spreadsheet.getCell(1, 1).getName(), "B2");
    }
    
    @Test
    public void nameOfSpreedsheetAddedCorrectly() {
        assertEquals(spreadsheet.getName(),newName);
    }
    
    @Test
    public void getValueOfEmptyCell() {
        assertEquals(spreadsheet.getCell(1, 1).getSrc(),"");
    }

    @Test
    public void createMoreThanOneCell() throws Exception {
        spreadsheet.addCell(1, 2, new Cell());
        spreadsheet.addCell(2, 1, new Cell());
        assertEquals(spreadsheet.getCell(1, 1).getName(), "B2");
        assertEquals(spreadsheet.getCell(1, 2).getName(), "B3");
        assertEquals(spreadsheet.getCell(2, 1).getName(), "C2");
    }

}
