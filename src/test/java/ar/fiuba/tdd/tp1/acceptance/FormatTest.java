package ar.fiuba.tdd.tp1.acceptance;

//import ar.fiuba.tdd.tp1.acceptance.driver.BadFormatException;
import ar.fiuba.tdd.tp1.SpreadsheetTestDriverImplementation;
import ar.fiuba.tdd.tp1.UndeclaredWorkSheetException;
import ar.fiuba.tdd.tp1.acceptance.driver.SpreadSheetTestDriver;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FormatTest {

    private SpreadSheetTestDriver testDriver;

    @Before
    public void setUp() {
        testDriver = new SpreadsheetTestDriverImplementation();
    }
    
//    @Test(expected = BadFormatException.class)
//    public void stringIsNotNumber() {
//        testDriver.createNewWorkBookNamed("tecnicas");
//        testDriver.setCellValue("tecnicas", "default", "A1", "2");
//        testDriver.setCellType("tecnicas", "default", "A1", "String");
//        testDriver.getCellValueAsDouble("tecnicas", "default", "A1");
//    }
    
    @Test
    public void formulaAsString() throws UndeclaredWorkSheetException {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 + 2");
        testDriver.setCellType("tecnicas", "default", "A1", "String");
        assertEquals("= 1 + 2", testDriver.getCellFormulaAsString("tecnicas", "default", "A1"));
    }

    @Test
    public void dateFormatEuropean() throws UndeclaredWorkSheetException {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "2012-07-14T00:00:00Z");
        testDriver.setCellType("tecnicas", "default", "A1", "Date");
        testDriver.setCellFormatter("tecnicas", "default", "A1", "format", "DD-MM-YYYY");
        assertEquals("14-07-2012", testDriver.getCellValueAsString("tecnicas", "default", "A1"));
    }

    @Test
    public void dateFormatAmerican() throws UndeclaredWorkSheetException {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "2012-07-14T00:00:00Z");
        testDriver.setCellType("tecnicas", "default", "A1", "Date");
        testDriver.setCellFormatter("tecnicas", "default", "A1", "format", "MM-DD-YYYY");
        assertEquals("07-14-2012", testDriver.getCellValueAsString("tecnicas", "default", "A1"));
    }

    @Test
    public void dateFormatISO8601() throws UndeclaredWorkSheetException {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "2012-07-14T00:00:00Z");
        testDriver.setCellType("tecnicas", "default", "A1", "Date");
        testDriver.setCellFormatter("tecnicas", "default", "A1", "format", "YYYY-MM-DD");
        assertEquals("2012-07-14", testDriver.getCellValueAsString("tecnicas", "default", "A1"));
    }
    
    @Test
    public void dateNoAutoformat() throws UndeclaredWorkSheetException {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "2012-07-14T00:00:00Z");
        assertEquals("2012-07-14T00:00:00Z", testDriver.getCellValueAsString("tecnicas", "default", "A1"));
    }
    
    @Test
    public void badDateFormat() throws UndeclaredWorkSheetException {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "Not a date");
        testDriver.setCellType("tecnicas", "default", "A1", "Date");
        testDriver.setCellFormatter("tecnicas", "default", "A1", "format", "YYYY-MM-DD");
        assertEquals("Error:BAD_DATE", testDriver.getCellValueAsString("tecnicas", "default", "A1"));
    }

    @Test
    public void moneyFormatUSD() throws UndeclaredWorkSheetException {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "2");
        testDriver.setCellType("tecnicas", "default", "A1", "Currency");
        testDriver.setCellFormatter("tecnicas", "default", "A1", "symbol", "U$S");
        assertEquals("U$S 2", testDriver.getCellValueAsString("tecnicas", "default", "A1"));
    }

    @Test
    public void moneyFormatUSDAndDecimal() throws UndeclaredWorkSheetException {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "2");
        testDriver.setCellType("tecnicas", "default", "A1", "Currency");
        testDriver.setCellFormatter("tecnicas", "default", "A1", "symbol", "U$S");
        testDriver.setCellFormatter("tecnicas", "default", "A1", "decimal", "2");
        assertEquals("U$S 2.00", testDriver.getCellValueAsString("tecnicas", "default", "A1"));
    }
    
    @Test
    public void badMoneyFormat() throws UndeclaredWorkSheetException {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "Not money");
        testDriver.setCellType("tecnicas", "default", "A1", "Currency");
        testDriver.setCellFormatter("tecnicas", "default", "A1", "symbol", "U$S");
        assertEquals("U$S Error:BAD_CURRENCY", testDriver.getCellValueAsString("tecnicas", "default", "A1"));
    }
}
