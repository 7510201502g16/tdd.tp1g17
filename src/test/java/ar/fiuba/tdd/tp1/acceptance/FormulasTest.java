package ar.fiuba.tdd.tp1.acceptance;

import ar.fiuba.tdd.tp1.BadFormulaException;
import ar.fiuba.tdd.tp1.BadReferenceException;
import ar.fiuba.tdd.tp1.SpreadsheetTestDriverImplementation;
import ar.fiuba.tdd.tp1.UndeclaredWorkSheetException;
import ar.fiuba.tdd.tp1.acceptance.driver.SpreadSheetTestDriver;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FormulasTest {

    private static final double DELTA = 0.0001;
    private SpreadSheetTestDriver testDriver;

    @Before
    public void setUp() {
        testDriver = new SpreadsheetTestDriverImplementation();
    }

    @Test
    public void sumLiterals() throws UndeclaredWorkSheetException {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 + 0 + 3.5 + -1");

        assertEquals(3.5, testDriver.getCellValueAsDouble("tecnicas", "default", "A1"), DELTA);
    }

    @Test
    public void sumLiteralsInDifferentRows() throws UndeclaredWorkSheetException {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "1");
        testDriver.setCellValue("tecnicas", "default", "B2", "2");
        testDriver.setCellValue("tecnicas", "default", "C3", "= A1 + B2");

        assertEquals(1 + 2, testDriver.getCellValueAsDouble("tecnicas", "default", "C3"), DELTA);
    }

    @Test
    public void subtractLiterals() throws UndeclaredWorkSheetException {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 - 0 - 3.5 - -1");

        assertEquals(1 - 3.5 - -1, testDriver.getCellValueAsDouble("tecnicas", "default", "A1"),
                DELTA);
    }

    @Test
    public void formulaWithReferences() throws UndeclaredWorkSheetException {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 + A2 + 0.5 - A3");
        testDriver.setCellValue("tecnicas", "default", "A2", "5");
        testDriver.setCellValue("tecnicas", "default", "A3", "2");

        assertEquals(1 + 5 + 0.5 - 2, testDriver.getCellValueAsDouble("tecnicas", "default", "A1"),
                DELTA);
    }

    @Test
    public void formulaWithReferenceToReference() throws UndeclaredWorkSheetException {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 + A2");
        testDriver.setCellValue("tecnicas", "default", "A2", "= 2 + A3");
        testDriver.setCellValue("tecnicas", "default", "A3", "3");

        assertEquals(1 + 2 + 3, testDriver.getCellValueAsDouble("tecnicas", "default", "A1"), DELTA);
    }

    @Test
    public void formulaWithReferencesFromOtherSpreadSheet() throws UndeclaredWorkSheetException {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.createNewWorkSheetNamed("tecnicas", "other");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 + other.A2 + 0.5 - A3");
        testDriver.setCellValue("tecnicas", "other", "A2", "-1");
        testDriver.setCellValue("tecnicas", "default", "A3", "2");

        assertEquals(1 + -1 + 0.5 - 2,
                testDriver.getCellValueAsDouble("tecnicas", "default", "A1"), DELTA);
    }

    @Test(expected = BadFormulaException.class)
    public void badFormulaStringAndNumber() throws UndeclaredWorkSheetException {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 + hello");

        testDriver.getCellValueAsDouble("tecnicas", "default", "A1");
    }

    @Test(expected = BadFormulaException.class)
    public void badFormulaStringAndNumberRetrieveAsString() throws UndeclaredWorkSheetException {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 + hello");
        testDriver.getCellValueAsDouble("tecnicas", "default", "A1");
    }

    @Test(expected = BadFormulaException.class)
    public void badFormulaWithReference() throws UndeclaredWorkSheetException {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 + A2");
        testDriver.setCellValue("tecnicas", "default", "A2", "Hello");

        testDriver.getCellValueAsDouble("tecnicas", "default", "A1");
    }

    // esto está comentado porque una celda puede contener algo del tipo 1 +2 como un string.
//    @Test(expected = BadFormulaException.class)
//    public void badFormulaWithoutEqualSymbol() throws UndeclaredWorkSheetException {
//        testDriver.createNewWorkBookNamed("tecnicas");
//        testDriver.setCellValue("tecnicas", "default", "A1", "1 + 2");
//
//        testDriver.getCellValueAsDouble("tecnicas", "default", "A1");
//    }

    @Test(expected = UndeclaredWorkSheetException.class)
    public void undeclaredWorkSheetInvocation() throws UndeclaredWorkSheetException {
        testDriver.createNewWorkBookNamed("tecnicas");

        testDriver.getCellValueAsString("tecnicas", "undeclaredWorkSheet", "A1");
    }

    @Test(expected = BadReferenceException.class)
    public void formulaWithCyclicReferences() throws UndeclaredWorkSheetException {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 + A2");
        testDriver.setCellValue("tecnicas", "default", "A2", "= A3");
        testDriver.setCellValue("tecnicas", "default", "A3", "= A1");
        testDriver.getCellValueAsDouble("tecnicas", "default", "A1");
    }

    @Test
    public void formulaWithChangesInPreviousCells() throws UndeclaredWorkSheetException {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 ");
        testDriver.setCellValue("tecnicas", "default", "A2", "= 8 + A1");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 10");
        assertEquals(8 + 10, testDriver.getCellValueAsDouble("tecnicas", "default", "A2"), DELTA);
    }

    @Test
    public void formulaWithAverageRangeCalculations() throws UndeclaredWorkSheetException {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "10 ");
        testDriver.setCellValue("tecnicas", "default", "A2", "80");
        testDriver.setCellValue("tecnicas", "default", "A3", "10");
        testDriver.setCellValue("tecnicas", "default", "A4", "20");
        testDriver.setCellValue("tecnicas", "default", "A5", "= AVERAGE(A1:A4)");
        assertEquals((10 + 80 + 10 + 20) / 4,
                testDriver.getCellValueAsDouble("tecnicas", "default", "A5"), DELTA);
    }

    @Test
    public void formulaWithMaxRangeCalculations() throws UndeclaredWorkSheetException {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "10 ");
        testDriver.setCellValue("tecnicas", "default", "A2", "80");
        testDriver.setCellValue("tecnicas", "default", "A3", "10");
        testDriver.setCellValue("tecnicas", "default", "A4", "20");
        testDriver.setCellValue("tecnicas", "default", "A5", "= MAX(A1:A4)");
        assertEquals(80, testDriver.getCellValueAsDouble("tecnicas", "default", "A5"), DELTA);
    }

    @Test
    public void formulaWithMinRangeCalculations() throws UndeclaredWorkSheetException {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "10 ");
        testDriver.setCellValue("tecnicas", "default", "A2", "80");
        testDriver.setCellValue("tecnicas", "default", "A3", "10");
        testDriver.setCellValue("tecnicas", "default", "A4", "20");
        testDriver.setCellValue("tecnicas", "default", "A5", "= MIN(A1:A4)");
        assertEquals(10, testDriver.getCellValueAsDouble("tecnicas", "default", "A5"), DELTA);
    }

    @Test
    public void formulaWithConcatCells() throws UndeclaredWorkSheetException {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "La edad es de ");
        testDriver.setCellValue("tecnicas", "default", "A2", "80");
        testDriver.setCellValue("tecnicas", "default", "A3", " años");
        testDriver.setCellValue("tecnicas", "default", "A5", "= CONCAT(A1,A2,A3)");
        assertEquals("La edad es de 80.0 años",
                testDriver.getCellValueAsString("tecnicas", "default", "A5"));
    }

    @Test
    public void formulaWithConcatCellsWithChangesInPreviousCells()
            throws UndeclaredWorkSheetException {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "La edad es de ");
        testDriver.setCellValue("tecnicas", "default", "B2", "80");
        testDriver.setCellValue("tecnicas", "default", "A2", "=B2");
        testDriver.setCellValue("tecnicas", "default", "A3", " años");
        testDriver.setCellValue("tecnicas", "default", "A5", "= CONCAT(A1,A2,A3)");
        testDriver.setCellValue("tecnicas", "default", "B2", "81");
        assertEquals("La edad es de 81.0 años",
                testDriver.getCellValueAsString("tecnicas", "default", "A5"));
    }

    @Test
    public void formulaWithDateFormat1() throws UndeclaredWorkSheetException {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "2012-07-14T00:00:00Z");
        testDriver.setCellType("tecnicas", "default", "A1", "Date");
        testDriver.setCellFormatter("tecnicas", "default", "A1", "format", "DD-MM-YYYY");
        assertEquals("14-07-2012", testDriver.getCellValueAsString("tecnicas", "default", "A1"));
    }

    @Test
    public void formulaWithDateFormat2() throws UndeclaredWorkSheetException {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "2012-07-14T00:00:00Z");
        testDriver.setCellType("tecnicas", "default", "A1", "Date");
        testDriver.setCellFormatter("tecnicas", "default", "A1", "format", "MM-DD-YYYY");
        assertEquals("07-14-2012", testDriver.getCellValueAsString("tecnicas", "default", "A1"));
    }

    @Test
    public void formulaWithMoneyFormatUSD() throws UndeclaredWorkSheetException {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "2");
        testDriver.setCellType("tecnicas", "default", "A1", "Currency");
        testDriver.setCellFormatter("tecnicas", "default", "A1", "symbol", "U$S");
        assertEquals("U$S 2", testDriver.getCellValueAsString("tecnicas", "default", "A1"));
    }

    @Test
    public void formulaWithMoneyFormatUSDAndDecimal() throws UndeclaredWorkSheetException {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "2");
        testDriver.setCellType("tecnicas", "default", "A1", "Currency");
        testDriver.setCellFormatter("tecnicas", "default", "A1", "symbol", "U$S");
        testDriver.setCellFormatter("tecnicas", "default", "A1", "decimal", "2");
        assertEquals("U$S 2.00", testDriver.getCellValueAsString("tecnicas", "default", "A1"));
    }

}
