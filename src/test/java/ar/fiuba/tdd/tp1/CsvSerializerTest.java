package ar.fiuba.tdd.tp1;

import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.Scanner;

import static org.junit.Assert.assertEquals;

public class CsvSerializerTest {
    private MainSpreadSheet mss;
    private String docName;
    private String serializeContent;
    private String fileContent;

    @Before
    public void initialization() {
        mss = new MainSpreadSheet();
        docName = "doc6.txt";
        mss.createDocument(docName);
    }

    @Test
    public void serializeDocWithManyCells() throws Exception {
        mss.createSheet(docName, "hoja1");
        mss.createCell(1, 1, "$20");
        mss.createCell(2, 3, "10");
        mss.createCell(2, 0, "hola");

        serializeContent = mss.serializeCSV(docName);
        fileContent = new Scanner(new File(docName)).useDelimiter("\\Z").next();
        assertEquals(serializeContent, fileContent);
    }

    @Test
    public void deserializeDocWithManyCells() throws Exception {
        mss.deserializeCSV("doc7.txt", "doc7.txt");
        assertEquals(mss.getActualDoc().getSpreadsheet("default").getName().toString(), "default");
        assertEquals(mss.getActualDoc().getSpreadsheet("default").getCell(2, 0).getSrc(),"hola");
        assertEquals(mss.getActualDoc().getSpreadsheet("default").getCell(2, 3).getSrc(),"10.0");
        assertEquals(mss.getActualDoc().getSpreadsheet("default").getCell(1, 1).getSrc(),"$20");
    }
}
