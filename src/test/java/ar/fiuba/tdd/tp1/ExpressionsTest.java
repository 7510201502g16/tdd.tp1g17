package ar.fiuba.tdd.tp1;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ExpressionsTest {
    private Number one;
    private Number two;
    private Number three;
    private Number four;

    @Before
    public void initialization() {
        one = new Number(1);
        two = new Number(2);
        three = new Number(3);
        four = new Number(4);
    }

    @Test
    public void simpleAdd() {
        // 1 + 2 = 3
        BinaryOperatorExpression simpleAdd = BinaryOperatorExpression.add(one, two);
        assertEquals((int)simpleAdd.eval(),3);
    }

    @Test
    public void simpleSub() {
        // 1 - 2 = -1
        BinaryOperatorExpression simpleSub = BinaryOperatorExpression.sub(one, two);
        assertEquals((int)simpleSub.eval(),-1);
    }

    @Test
    public void complexSum() {
        //  1 + 2 + 3 + 4
        BinaryOperatorExpression add1 = BinaryOperatorExpression.add(one, two);
        BinaryOperatorExpression add2 = BinaryOperatorExpression.add(add1, three);
        BinaryOperatorExpression add3 = BinaryOperatorExpression.add(add2, four);
        assertEquals((int)add3.eval(),10);
    }
}